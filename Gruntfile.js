module.exports = function(grunt) {
	grunt.initConfig({
		//Tarefa de copia
		copy: {
			public: {
				expand: true, //Mapeamento dinamico
				cwd: "public_html", //Diretorio padrao
				src: ['**',  '!**/less/**'], //Todos arquivos ** = todos  exceto a pasta less
				dest: "dist" //Pasta de destino cópia
			}
		},
	
		//Tarefa de delete
		clean: {
			dist: {
				src: "dist" //diretorio que deleta
			}
		},
	
		//Tarefa de concatenacao e minificação
		useminPrepare: {
			html: "dist/**/*.html" //acho que varre o html 
		},
		usemin: {
			html: "dist/**/*.html" //Muda a chamada <link> pro arquivo novo "min"
		},
		
		//Otimização de imagens  
		imagemin: {
			public: {
				expand: true, //Mapeamento dinamico
				cwd: "dist/images", //Diretorio padrao
				src: "**/*.{png}", //Todos arquivos ** = todos  
				dest: "dist/images" //Pasta de destino cópia
			}
		},
		
		//Renomeia os arquivos pra nao ter problema de cache
		rev: {
			options: {
				encoding: "uft8",
				algorithm: "md5",
				length: 10
			},
//			imagens: {
//				src: ["dist/images/**/*.{png,jpg,gif,svg}"]
//			},
			minificados: {
				src: ["dist/js/**/*.min.js", "dist/css/**/*.min.css"]
			}
		},
		
		//Less
		less: {
			compilar: {
				expand: true,
				cwd: "public_html/less", //pasta onde estão os arquivos less
				src: ["**/*.less"], //todos arquivos .less
				dest: "public_html/css", //pasta para onde vai o css compilado
				ext: ".css" 
			}
		},
		
		//Task que fica olhando arquivos less para compilar
		watch: {
			less: {
				options: {
					event: ["added", "changed"]
				},
				files: "public_html/less/**/*.less",
				tasks: "less:compilar"
			}
		}
	});
	
	//Criando nossa própria tarefa que chama várias tarefas
	grunt.registerTask("dist", ["clean", "copy"]);
	grunt.registerTask("minifica", ["useminPrepare", "cssmin", "rev", "usemin", "concat", "imagemin"]); //tirei porque nao to usando uglify depois de concat //
	
	//Tarefa default é só chamar "grunt" no terminal, que ela chama dist, que chama todas...
	grunt.registerTask("default", ["dist", "minifica"]);
	
	//Plugins
	grunt.loadNpmTasks("grunt-contrib-copy"); //Plugin de cópia
	grunt.loadNpmTasks("grunt-contrib-clean"); //Plugin que deleta pasta
	grunt.loadNpmTasks("grunt-contrib-concat"); //Plugin concatena todas palavras do arquivo
	grunt.loadNpmTasks("grunt-contrib-uglify"); //Plugin concatena scripts js e gera min.js
	grunt.loadNpmTasks("grunt-contrib-cssmin"); //Plugin concatena arquivos css min.css
	grunt.loadNpmTasks("grunt-usemin"); //Plugin que facilita os 3 plugins (concat, uglify, cssmin)
	grunt.loadNpmTasks("grunt-contrib-imagemin"); //Plugin que facilita os 3 plugins (concat, uglify, cssmin)
	grunt.loadNpmTasks("grunt-rev"); //Plugin que renomeia os arquivos com o hash md5
	grunt.loadNpmTasks("grunt-contrib-less"); //Plugin less
	grunt.loadNpmTasks("grunt-contrib-watch"); //Plugin que fica verificando arquivos less

	/*
	Na pasta do projeto

		npm init
		npm install grunt --save-dev
		sudo npm install grunt -cli -g
		npm install
		npm install grunt-contrib-copy --save-dev
		npm install grunt-contrib-clean --save-dev
		npm install grunt-contrib-concat --save-dev
		npm install grunt-contrib-uglify --save-dev
		npm install grunt-contrib-cssmin --save-dev
		npm install grunt-usemin --save-dev
		npm install grunt-contrib-imagemin --save-dev
		npm install grunt-rev --save-dev
		npm install grunt-contrib-less --save-dev
		npm install grunt-contrib-watch --save-dev	
		
	*/
	
}