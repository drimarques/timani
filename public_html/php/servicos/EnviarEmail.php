<?php
function carregaClasses($nomeClasse){
	if(file_exists("../classes/".$nomeClasse.".php")){
		require_once "../classes/".$nomeClasse.".php";
	}
}
spl_autoload_register("carregaClasses");

$nome = $_POST["txt-nome"];
$sobrenome = $_POST["txt-sobrenome"];
$telefone = $_POST["txt-telefone"];
$email = $_POST["txt-email"];
$empresa = $_POST["txt-empresa"];
$funcionarios = $_POST["txt-funcionarios"];
$cidade = $_POST["txt-cidade"];
$uf = $_POST["txt-uf"];
$mensagem = $_POST["txt-mensagem"];

$mensagemEmail = "<html>
					<head>
						<meta charset='UTF-8'>
						<title>Contato - TIMANI</title>
						<meta name='viewport' content='width=device-width, initial-scale=1.0' />
						<style>										
						</style>				
					</head>
					<body>
						<div style='border-radius: 30px; border: solid 1px #5CC1A4; padding: 40px 30px; width: 85%; margin: 0 auto'>
							<div style='margin-bottom: 25px;'>
								<table style='width: 100%; border-collapse: collapse; background-color: #FFF;'>
									<tr>
										<td style='width: 100%; text-align: center'><img style='width: 200px; height: auto' src='http://www.timani.com.br/dev/images/logo-timani.png' alt='TIMANI'></td>
									</tr>
								</table>
							</div>
							<div style='clear: both'></div>
							<div style='margin-bottom: 25px; margin-top: 40px;'>
								<p style='font-size: 15px; font-family: Arial, sans-serif; color: #232121'>Olá Anderson!</p>
								<p style='font-size: 15px; font-family: Arial, sans-serif; color: #232121'>Recebemos um e-mail de contato do site:</p>
							</div>
							<div>
								<p style='font-size: 13px; font-family: Arial, sans-serif; color: #232121'>Nome: {$nome} {$sobrenome}</p>
								<p style='font-size: 13px; font-family: Arial, sans-serif; color: #232121'>Telefone: {$telefone}</p>
								<p style='font-size: 13px; font-family: Arial, sans-serif; color: #232121'>E-mail: {$email}</p>
								<p style='font-size: 13px; font-family: Arial, sans-serif; color: #232121'>Empresa: {$empresa}</p>
								<p style='font-size: 13px; font-family: Arial, sans-serif; color: #232121'>Funcionários: {$funcionarios}</p>
								<p style='font-size: 13px; font-family: Arial, sans-serif; color: #232121'>Cidade: {$cidade}</p>
								<p style='font-size: 13px; font-family: Arial, sans-serif; color: #232121'>Estado: {$uf}</p>
								<p style='font-size: 13px; font-family: Arial, sans-serif; color: #232121'>Mensagem: {$mensagem}</p>
							</div>
						</div>
					</body>
				</html>";


//echo $mensagemEmail;

$mail = new EnvioEmail();
$mail->adicionaDestinatario("anderson.tiburcio@gmail.com");
$mail->setAssunto("TIMANI - Contato site");
$mail->setMensagem($mensagemEmail);

if($mail->enviaEmail()){
	echo "success";
}
