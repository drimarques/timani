$(function(){

	$('.bxslider').bxSlider({
	  mode: 'fade',
	  auto: true,
	  autoControls: true,
	  speed: 500
	});
	
	$('a.scroll-automatico').bind('click', function(e) {
		var $anchor = $(this);
		var menu_clicado = this;

		$('html, body').stop().animate({
			scrollTop : $($anchor.attr('href')).offset().top
		}, 
		1000, 
		'easeInOutExpo', 
		function() {
		});
		e.preventDefault();
	});
	
	$("#form-contato").submit(function(e) {
		e.preventDefault();
		
		$("#mensagem-email").fadeIn("fast");
		
		var nome = $("#txt-nome").val();
		var sobrenome = $("#txt-sobrenome").val();
		var telefone = $("#txt-telefone").val();
		var email = $("#txt-email").val();
		var empresa = $("#txt-empresa").val();
		var funcionario = $("#txt-funcionarios").val();
		var cidade = $("#txt-cidade").val();
		var uf = $("#txt-uf").val();
		var mensagem = $("#txt-mensagem").val();
		
		$.ajax({
			url : "php/servicos/EnviarEmail.php",
			data : {
				"txt-nome": nome,
				"txt-sobrenome": sobrenome,
				"txt-telefone": telefone,
				"txt-email": email,
				"txt-empresa": empresa,
				"txt-funcionarios": funcionario,
				"txt-cidade": cidade,
				"txt-uf": uf,
				"txt-mensagem": mensagem
			},
			type : 'POST',
			success : function(retorno) {
				console.log(retorno);
				if(retorno == "success"){
					$("#mensagem-email p").html("Obrigado!<br>Recebemos seu e-mail, em breve entraremos em contato.<br> Até logo ;)");
					
					setTimeout(function(){
						$("#mensagem-email").fadeOut("fast");
						$("#mensagem-email p").html("Por favor, aguarde...");
					}, 6000);	
				}
			},
			error : function(retorno) {
				
			}
		});
	});
});